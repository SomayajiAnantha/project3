a=3
b=4
c=7

print(a)
print(b)
print(c)    # here printing values of a, b,c are done vertically down

print( a, end=' ')
print(b, end=' ')
print(c)    # here printing values of a, b,c are done horizontal
