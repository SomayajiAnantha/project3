s="I am the best person in the world"

# string  is -   I am the best person in the world
# indexing   -   0123456789----------------------32
# revesre index-                       -4 -3 -2 -1
#                                       o  r  l  d
print(s)

# finding the length of the string

print(len(s))

# [] - slicing operator

print (s[0])

print(s[-1])

print(s[32])

print(s[-3])

print(s[-33])

# range and slice operator [:]

print(s[5:10])
print(s[10:-5])

print(s[-20:-10])

print(s[:20])

print(s[10:])

print(s[:])

s1=" you are next"

print(s+s1)

print(s*2)

# s[0]= 'y'

s= " Sorry, you are best"

print(s)

print('e' in s)

print('z' in s)

print('z' not in s)

print('e' not in s)

a= 'z' not in s

print(a)

# iterator

for i in s:
    print(i)

for i in s:
    if i== 'a':
        break
    print(i, end='')

for i in range( 1,5):
    print(i)
else: print (" the for loop completed")

while(i>0):
    print(i)
    i-=1
else: print(' while loop exhuasted')

for i in range (1,11):
    if(i%2 !=0):
        pass    # not doing anything
        print(' pass executed')
    else:
        print(i)

print(s.upper())
print(s.lower())

# a new comment
#another comment















